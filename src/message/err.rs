#[cfg(feature = "irc")]
use irc;
#[cfg(feature = "pircolate")]
use pircolate;
use std::error;
use std::str;
use std::string;

error_chain! {
    foreign_links {
        Utf8Error(str::Utf8Error);
        FromUtf8Error(string::FromUtf8Error);
    }

    links {
        IrcCrate(irc::error::Error, irc::error::ErrorKind)
            #[cfg(feature = "irc")];
        Pircolate(pircolate::error::Error, pircolate::error::ErrorKind)
            #[cfg(feature = "pircolate")];
    }

    errors {
        Other(inner: Box<error::Error + Send>) {
            description("there was an unspecified problem with an IRC message")
            display("{}", inner)
        }
    }
}
